import Test.QuickCheck

-- TODO:  the tree generation in the example is using 'sized' and 'resize'

-- From e7, needed for this exercise.\

data Stream a = Cons a (Stream a)

streamToList :: Stream a -> [a]
streamToList (Cons x rest) = x:streamToList rest

streamMap :: (a -> b) -> Stream a -> Stream b
streamMap f (Cons x rest) = Cons (f x) (streamMap f rest)

streamIterate :: (a -> a) -> a -> Stream a
streamIterate f seed = Cons seed $ streamIterate f $ f seed

streamInterleave :: Stream a -> Stream a -> Stream a
streamInterleave (Cons a arest) bstream = Cons a (streamInterleave bstream arest)

streamRepeat :: a -> Stream a
streamRepeat x = Cons x $ streamRepeat x

nats :: Stream Integer
nats =  streamIterate (+ 1) 1

ruler :: Stream Integer
ruler = go 0 where
  go x = streamInterleave (streamRepeat x) (go (x + 1))

instance Show a => Show (Stream a) where
  show = show . take 40 . streamToList 

data Supply s a = S (Stream s -> (a, Stream s))

get :: Supply s s
get = S (\(Cons s ss) -> (s, ss))

pureSupply :: a -> Supply s a
pureSupply foo = S (\s -> (foo, s))

runSupply :: Stream s -> Supply s a -> a
runSupply stream (S sf) = fst $ sf stream

mapSupply :: (a -> b) -> Supply s a -> Supply s b
mapSupply f (S t) = S $ tupleMap (f, id)  . t 
  where tupleMap (f1, f2) (x1, x2) = (f1 x1, f2 x2)

-- is this inneficiant since it calculate t s twice?
mapSupplyx :: (a -> b) -> Supply s a -> Supply s b
mapSupplyx f (S t) = S ( \s -> (f ((fst . t) s), (snd . t) s))

-- run the supply thing twice .. using output as input to next.
-- this is the correct way per example
mapSupply2 :: (a -> b -> c) -> Supply s a -> Supply s b -> Supply s c
mapSupply2 f (S t) (S u) = S v
  where v s = let (x, s') = t s
                  (x', s'') = u s'
                  in  (f x x', s'')

-- is this wrong since it ignores the snd part of intermediate result?
mapSupply2x :: (a -> b -> c) -> Supply s a -> Supply s b -> Supply s c
mapSupply2x f (S t) (S u) = S (\s ->  ( f (tval s) (uval s), rest s) )
  where
    tval =  fst . t  
    uval =  fst . u 
    rest (Cons s ss) = ss -- using 'rest' and ignoring t and u 

--mapSupply2 = undefined
bindSupply :: Supply s a -> (a -> Supply s b) -> Supply s b
bindSupply (S sfa) f = S fsb'
  where fsb' s = let (a,s') = sfa s
                     (S fsb) = f a
                      in fsb s'

instance Functor (Supply s) where
    fmap = mapSupply

instance Applicative (Supply s) where
    pure = pureSupply
    (<*>) = mapSupply2 id

instance Monad (Supply s) where
    return = pureSupply
    (>>=) = bindSupply

data Tree a = Node (Tree a) (Tree a) | Leaf a deriving Show

labelTree :: Tree a -> Tree Integer
labelTree t = runSupply nats (go t)
  where go :: Tree a -> Supply s (Tree s)
        go (Leaf _ ) = Leaf <$> get -- mapSupply Leaf get 
        go (Node l r) = Node <$> go l <*> go r  --mapSupply2 Node (go l) (go r)

-- Exercise 1

instance Arbitrary a => Arbitrary (Tree a) where 
    arbitrary = genTree

genTree :: Arbitrary a => Gen (Tree a)
genTree = oneof [ genNode, genLeaf ] where
    genNode = Node <$> genTree <*> genTree
    genLeaf = Leaf <$> arbitrary


-- Exercise 2
size :: Tree a -> Int
size (Leaf _) = 1
size ( Node l r ) = size l + size r

toList :: Tree a -> [a]
toList (Leaf x ) = [x]
toList (Node l r ) = toList l ++ toList r



prop_lengthToList :: Tree Integer -> Bool
--    The length of the list produced by toList is the size of the given tree.
prop_lengthToList t =  size t == length ( toList t )

prop_sizeLabelTree :: Tree Integer -> Bool
prop_sizeLabelTree t = size t == size  ( labelTree t )
--    labelTree does not change the size of the tree.

prop_labelTree :: Tree Integer -> Bool
-- For every tree t, toList (labelTree t) is the expected list.
-- Hint: [0..n] denotes the list of numbers from 0 to n, inclusively.
prop_labelTree t = toList t == [0..(toInteger (size t))] where

instance (Eq a ) => Eq (Tree a) where
    (Leaf a) == (Leaf b) = a == b
    (Node l r) == ( Node l2 r2 ) = l == l2 && r == r2
    _ == _ = False

prop_labelTreeIdempotent :: Tree Integer -> Bool
-- Applying labelTree to a list twice does yield the same list as applying it once.
prop_labelTreeIdempotent t =  labelTree t == ( labelTree t )

main :: IO ()
main = do
    quickCheck prop_lengthToList
    quickCheck prop_sizeLabelTree
    quickCheck prop_labelTree
    quickCheck prop_labelTreeIdempotent

