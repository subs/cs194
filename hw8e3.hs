import Data.Char

data Parser a = P (String -> Maybe (a, String))

-- two following sigs with/withoug () seem to be same
--runParser :: Parser a -> (String -> (Maybe a, String))
runParser :: Parser a -> String -> Maybe (a, String)
runParser (P p) = p

-- how to curry it?
parse :: Parser a -> String -> Maybe a
parse pa input =  fst <$> runParser pa input

noParser :: Parser a
noParser = P (\_ -> Nothing)

pureParser :: a -> Parser a
pureParser xxx = P (\input -> Just (xxx, input))

-- this is better than the anwser cause it doesn't have the cases for Maybe instead uses <$>
-- but is there a way without maptuple?
instance Functor Parser where
    fmap f (P pf) = P $ \s -> do
        (x, s') <- pf s
        return (f x, s')

instance Applicative Parser where
    --(<*>) = fmap2 id
    pure = pureParser 
    -- there was even something wrong with my explicity case fmap2  because 'many' then looped
    pf <*> pp = P $ \s -> do
        (f, s') <- runParser pf s
        (x, s'') <- runParser pp s'
        return (f x, s'')

bindParser :: Parser a -> (a -> Parser b) -> Parser b
bindParser (P pa) f = (P pb') where
    pb' s = let processed = pa s
            in case processed of Nothing -> Nothing
                                 Just (a, s') -> let (P pb) = f a
                                                 in pb s'
                              
instance Monad Parser where
    return = pureParser
    (>>=) = bindParser

anyChar :: Parser Char
anyChar = P anyCharF where
    anyCharF (c:s) = Just (c, s)
    anyCharF _ = Nothing

-- need to use return and noParser
char :: Char -> Parser ()
char c = P charF where
    charF (x:s) | c == x = Just ((), s)
    charF _ = Nothing

anyCharBut :: Char -> Parser Char
anyCharBut c = do 
    c' <- anyChar 
    if c == c' then noParser else return c' 

-- use runParser and anonymous functions better
--not as good... 
orElse'' :: Parser a -> Parser a -> Parser a
orElse'' (P t1) (P t2) = P p
    where p s = let test = t1 s
                    in case test of Nothing -> t2 s
                                    _ -> test

-- correct... can it be done with a do block?
orElse :: Parser a -> Parser a -> Parser a
orElse t1 t2 = P $ \s -> case runParser t1 s of
    Nothing -> runParser t2 s
    _ -> runParser t1 s

-- this is better:
many :: Parser a -> Parser [a]
many p = ( (:) <$> p <*>  many p ) `orElse` return [] 
-- this doesn't really use enough abstaction
--many :: Parser a -> Parser [a]
--many p =  P $ \s -> case runParser p s of
 --   Nothing -> runParser (pureParser []) s
  --  Just (a, s') -> do (a',s'') <- runParser (many p) s'
                 --      return ( a:a' , s'')

-- Both of these still add an extrta "" to the array... 
--  for some reason if the string ends with the separator
-- the first one is more consice
-- good idea to switch p2 and p1...
sepBy :: Parser a -> Parser ()  -> Parser [a] 
sepBy p1 p2 = ((:) <$> p1 <*> (many (p2 >> p1))) `orElse` return []

parseCSV :: Parser [[String]]
parseCSV = many parseLine
  where
    parseLine = parseCell `sepBy` char ',' <* char '\n'
    parseCell = do
        char '"'
        content <- many (anyCharBut '"')
        char '"'
        return content

type Identifer = String
type Declaration = (Identifer, String)
type Section = (Identifer, [Declaration])
type INIFile = [Section]

-- ! shoul use 'orElse'
parseINI :: Parser INIFile
parseINI = many parseSection
    where

        parseDeclaration :: Parser Declaration
        parseDeclaration = do
            k <- spaces *> (many (anyCharWhere isAlphaNum) ) <* spaces
            char '='
            v <- spaces *> (many (anyCharBut '\n')) <* char '\n'
            return (k,v)
      
        parseHeader :: Parser Identifer
        parseHeader = spaces *> char '[' *> (many (anyCharWhere isAlphaNum) ) <* char ']' <* spaces <* char '\n'

        parseSection :: Parser Section
        parseSection = do
            ignore
            k <- parseHeader
            ignore
            v <- many ( parseDeclaration `orElse` ignore ) 
            ignore
            return (k,v)
      

       -- ignore :: Parser [()]
        --ignore = many $ ( spaces `orElse` comment  ) >> char '\n'
        ignore = spaces  >> char '\n' >> noParser
       
            where comment = spaces >> char '#' >> many (anyCharBut '\n')  >> return [()]
      
        spaces :: Parser [()]
        spaces = many (char ' ')
      
        anyCharWhere :: (Char -> Bool) -> Parser Char
        anyCharWhere f = P anyCharF where
               anyCharF (c:s) | f c = Just (c, s)
               anyCharF _ = Nothing

        manySkipping :: Parser a -> Parser b -> Parser [b]
        manySkipping (P ignoreit) (P stuff) = many (P good) 
            where good input = case stuff input of Just x -> Just [x]
                                                   Nothing -> case ignoreit input of Just y -> Just []
                                                                                     _ -> Nothing

                                     






stuff = "[requests]\ndesiredFood = cookies\ndesiredQuantity = 20\n  \n \n[supply]\n\n#foobar\nflour = 20 ounzes\n\nsugar = none!\n[conclusion]"

main = do
    print "Ok"
    print $ parse anyChar ""
    print $ parse anyChar "a"
    print $ parse anyChar "qwerty"
    print $ parse ( anyCharBut 'x' ) "xyx"
    print $ parse ( anyCharBut 'x' ) "abc"
    print $ parse ( char 'b') "mutt"
    print $ parse ( orElse (char 'a') (char 'b') ) "butt"
    print $ parse ( many (anyCharBut 'z')) "xxxxxxxxy"
    print $ parse ( sepBy (many (anyCharBut 'X') ) (char 'X') ) "xxxxXasdafXadXasXa"
    print $ parse ( anyCharBut 'X') ""
    print $ lines "slfkdj\nsfdlkj\n"
    print $ parse ( anyCharBut 'X' *> char 'X') "a"
    -- print $ parse ( many ( sepBy1 (many (anyCharBut 'X') ) (char 'X') ) ) "sdffsddfXsXaXXaaaXbbb" 
    -- parse $ parse parseCSV $ "\"ab\",\"cd\"\n\"\",\"de\"\n\n"
    -- same as answer but answer has extra [] 
    print $ parse parseCSV "\"ab\",\"cd\"\n\n\"\",\"de\"\n\"a\",\"b\",\"c\"\n\n"
    print $ parse parseINI stuff