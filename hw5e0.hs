import Data.Char
import Data.List
import Data.Ord
import Data.Function
--

-- e1
halveEvens :: [Integer] -> [Integer]
halveEvens = map (\x -> div x 2) . filter even 

-- e2
safeString :: String -> String
safeString = filter safe
    where safe :: Char -> Bool
          safe x
            | isControl x = False
            | isAscii x = True
            | otherwise = False

-- e3
holes :: [a]->[[a]]
holes s = 
    map func iter
    where
        iter = [ 0..length s - 1 ]
        func n = (fst splat) ++ (drop 1 (snd splat) )
            where
                splat = splitAt n s
-- almost...
holes2 s =
    let top =  drop 1 $ inits s
        bot =  drop 1 $ tails s
        in zipWith (++) top bot

longestText :: Show a => [a] -> a
-- actual answer is soo much better:
-- longestText = maximumBy (comparing (length . show))
longestText tt = foldl maxtext (head tt) tt
    where
      maxtext a b | length (show a ) > length (show b) = a
                  | otherwise = b

adjacents :: [a] -> [(a,a)]
adjacents x = zip x $ tail x

commas :: [String] -> String
commas x = intercalate  ", " x  

addPolynomials :: [[Integer]] -> [Integer]
-- could use foldl1 to avoid the base case..
addPolynomials = foldl (zipWith (+)) (repeat 0) 

sumNumbers :: String -> Integer
--- notquite right since i didn't read it properly: sumNumbers = sum . map read . filter (isDigit.head) . groupBy ((==) `on` isDigit)
sumNumbers = toInteger . foldl (+) 0 . map digitToInt . filter isDigit 


-- e2
wordCount :: String -> String
wordCount txt = unlines [ goGetIt "Number of lines" lineCnt, 
                        goGetIt "Number of words" wordCnt, 
                        goGetIt "Number of empty lines" emptyLineCnt ,
                        goGetIt "Number of words followed by same word" (countMatchesNext . words ) ,
                        goGetIt "Longest line" longestLine ,
                        goGetIt "Longest line length" ( length . longestLine )  ,
                        goGetIt "Number of unique words" ( numUnique . words) ]
        where --goGetIt :: String -> (String -> Int) -> String
              goGetIt desc f = unwords [ desc ++ ":", show (f txt)]
              lineCnt   =  length . lines
              wordCnt   =  length . words
              emptyLineCnt  = length . filter isNotWhite . lines
              isNotWhite = foldl (\x y -> x && isSpace y) True
              -- Maybe not conscise but SO COOL.
              countMatchesNext = foldl (+) 0 . map (\(x,y) -> if x == y then 1 else 0) . (\s -> zip s (drop 1 s))
              longestLine = maximumBy ( comparing (length . show )) . lines
              -- much more concise: use 'nub'.  'nub' function is like ( sort . uniq )
              --  also use the adjacents defined above
              numUnique = (\s -> (length s) - (countMatchesNext $ sort s ) ) 




main :: IO ()
main = do
    putStrLn "test"
   --putStrLn $ wordCount "You're a  b b strange b  aninale\nthats  b what what whati i a  know\n\n\n\nIve got got got to follow"
    putStrLn $ formatTests $ testResults

formatTests :: [(String, [Bool])] -> String
formatTests = ( intercalate "\n" ) . (map showOneTest)
    where showOneTest (name, results) = name ++ ": " ++ show (length (filter (== True) results) ) ++ "/"++ show (length results)

-- e3
testResults :: [(String, [Bool])]
testResults = [ ("halveEvens",      ex_halveEvens)
              , ("safeString",      ex_safeString)
              , ("holes",           ex_holes)
              , ("longestText",     ex_longestText)
              , ("commas",          ex_commas)
              , ("addPolynomials",  ex_addPolynomials)
              , ("sumNumbers",      ex_sumNumbers)
              ]

ex_halveEvens =
    [ halveEvens [] == []
    , halveEvens [1,2,3,4,5] == [1,2]
    , halveEvens [6,6,6,3,3,3,2,2,2] == [3,3,3,1,1,1]
    ]

ex_safeString =
    [ safeString [] == []
    , safeString "Hello World!" == "Hello World!"
    , safeString "That’s your line:\n" == "That_s your line:_"
    , safeString "🙋.o(“Me Me Me”)" == "_.o(_Me Me Me_)"
    ]

ex_holes =
   [ holes "" == []
   , holes "Hello" == ["ello", "Hllo", "Helo", "Helo", "Hell"]
   ]

ex_longestText =
   [ longestText [True,False] == False
   , longestText [2,4,16,32] == (32::Int)
   , longestText (words "Hello World") == "World"
   , longestText (words "Olá mundo") ==  "Olá"
   ]

ex_adjacents =
   [ adjacents "" == []
   , adjacents [True] == []
   , adjacents "Hello" == [('H','e'),('e','l'),('l','l'),('l','o')]
   ]

ex_commas =
   [ commas [] == ""
   , commas ["Hello"] == "Hello"
   , commas ["Hello", "World"] == "Hello, World"
   , commas ["Hello", "", "World"] == "Hello, , World"
   , commas ["Hello", "new", "World"] == "Hello, new, World"
   ]

ex_addPolynomials =
   [ addPolynomials [[]] == []
   , addPolynomials [[0, 1], [1, 1]] == [1, 2]
   , addPolynomials [[0, 1, 5], [7, 0, 0], [-2, -1, 5]] == [5, 0, 10]
   ]

ex_sumNumbers =
   [ sumNumbers "" == 0
   , sumNumbers "Hello world!" == 0
   , sumNumbers "a1bc222d3f44" == 270
   , sumNumbers "words0are1234separated12by3integers45678" == 46927
   , sumNumbers "000a." == 0
   , sumNumbers "0.00a." == 0
   ]
