
-- e1
fibs2 :: [Integer]
fibs2 = 1:1:zipWith (+) fibs2 (drop 1 fibs2)

-- e2
data Stream a = Cons a (Stream a)

streamToList :: Stream a -> [a]
streamToList (Cons x rest) = x:streamToList rest

streamMap :: (a -> b) -> Stream a -> Stream b
streamMap f (Cons x rest) = Cons (f x) (streamMap f rest)

streamIterate :: (a -> a) -> a -> Stream a
streamIterate f seed = Cons seed $ streamIterate f $ f seed

streamInterleave :: Stream a -> Stream a -> Stream a
streamInterleave (Cons a arest) bstream = Cons a (streamInterleave bstream arest)

streamRepeat :: a -> Stream a
streamRepeat x = Cons x $ streamRepeat x

nats :: Stream Integer
nats =  streamIterate (+ 1) 1

ruler :: Stream Integer
ruler = go 0 where
  go x = streamInterleave (streamRepeat x) (go (x + 1))

instance Show a => Show (Stream a) where
  show = show . take 40 . streamToList 

-- e3

data Supply s a = S (Stream s -> (a, Stream s))

get :: Supply s s
get = S (\(Cons s ss) -> (s, ss))

pureSupply :: a -> Supply s a
pureSupply foo = S (\s -> (foo, s))

runSupply :: Stream s -> Supply s a -> a
runSupply stream (S sf) = fst $ sf stream

mapSupply :: (a -> b) -> Supply s a -> Supply s b
mapSupply f (S t) = S $ tupleMap (f, id)  . t 
  where tupleMap (f1, f2) (x1, x2) = (f1 x1, f2 x2)

-- is this inneficiant since it calculate t s twice?
mapSupplyx :: (a -> b) -> Supply s a -> Supply s b
mapSupplyx f (S t) = S ( \s -> (f ((fst . t) s), (snd . t) s))

-- run the supply thing twice .. using output as input to next.
-- this is the correct way per example
mapSupply2 :: (a -> b -> c) -> Supply s a -> Supply s b -> Supply s c
mapSupply2 f (S t) (S u) = S v
  where v s = let (x, s') = t s
                  (x', s'') = u s'
                  in  (f x x', s'')


-- is this wrong since it ignores the snd part of intermediate result?
mapSupply2x :: (a -> b -> c) -> Supply s a -> Supply s b -> Supply s c
mapSupply2x f (S t) (S u) = S (\s ->  ( f (tval s) (uval s), rest s) )
  where
    tval =  fst . t  
    uval =  fst . u 
    rest (Cons s ss) = ss -- using 'rest' and ignoring t and u 


--mapSupply2 = undefined

bindSupply :: Supply s a -> (a -> Supply s b) -> Supply s b
bindSupply (S fsa) f = S fsb'
  where fsb' s = let (a,s') = fsa s
                     (S fsb) = f a
                     in fsb s'     
                     

instance Functor (Supply s) where
    fmap = mapSupply

instance Applicative (Supply s) where
    pure = pureSupply
    (<*>) = mapSupply2 id

instance Monad (Supply s) where
    return = pureSupply
    (>>=) = bindSupply


data Tree a = Node (Tree a) (Tree a) | Leaf a deriving Show



t = let l = Leaf 1 ; n = Node in n (n (n l l) l) (n l l)


main :: IO ()
main = do
  print t
--  print $ labelTree t
