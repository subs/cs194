import Data.Maybe

-- Exercise 1: A Functor instance
data ComplicatedA a b
    = Con1 a b
    | Con2 [Maybe (a -> b)]

-- ?? why not instancd Functor (ComplicatedA a b ) where ...   ???????
instance Functor (ComplicatedA b ) where
   fmap f (Con1 a b) = Con1 a  (f b)
   fmap f (Con2 xs) = Con2 (map ( (f .) <$> ) xs)

data ComplicatedB f g a b
    = Con3 (f a)
    | Con4 (g b)  -- this is a g with type b, eg 'Supply b' .
    | Con5 (g (g [b]))
 
instance Functor g => Functor (ComplicatedB f g a) where
    fmap h (Con3 fa) = Con3 fa
    fmap h (Con4 gb) = Con4 $ h <$> gb
    fmap h (Con5 gglb) = Con5 $ ((map h) <$>) <$> gglb

-- does g need to be functor? 
-- i guess you cannot define the funcor part of g inline because you don't know what is the 
--  construcor of g. the following will give a parse error. Using a pattern match like 
--    fmapinline f (g b) = g $ f b
-- doesn't work because the 'g' is not actually the construcor it is the type variable.
-- eg, it is not 'S b' / ' Con2 b' but 'Supply b' / 'ComplicatedX b'  which doesn't work?

-- Exercise 2: rewriting modadic code
func0 :: Monad f => (a -> a) -> f a -> f a
func0 f xs = do
    x <- xs
    return (f (f x))

-- example:
func0' :: Functor f => (a -> a) -> f a -> f a
func0' f xs = (f . f) <$> xs


func1 :: Monad f => f a -> f (a,a)
func1 xs = xs >>= (\x -> return (x,x))

-- replacing 'f' with 'm' in the type signature because im configusing it with 'f' in the pattern match
func1' :: Functor m => m a -> m (a, a)
func1' xs = (\x -> (x, x)) <$> xs


func2 :: Monad f => f a -> f (a,a)
func2 xs = xs >>= (\x -> xs >>= \y -> return (x,y))

func2' :: Applicative m => m a -> m (a,a)
func2' xs = (,) <$> xs <*> xs
-- func2' xs = (\x  -> (x,x)) <$> xs -- why not work without the <*> xs


func3 :: Monad f => f a -> f (a,a)
func3 xs = xs >>= (\x -> xs >>= \y -> return (x,x))

func3' :: Applicative m => m a -> m (a, a)
-- how is this different from the correct? func3' xs = (,) <$> xs <*> xs
-- or :  --func3' xs = id <$> (\x -> (x,x)) <$> xs
func3' xs = (\x _ -> (x,x)) <$> xs <*> xs


func4 :: Monad f => f a -> f a -> f (a,a)
func4 xs ys = xs >>= (\x -> ys >>= \y -> return (x,y))

-- this is correct .. not quite sure why
func4' :: Applicative f => f a -> f a -> f (a, a)
func4' xs ys = (,) <$> xs <*> ys


func5 :: Monad f => f Integer -> f Integer -> f Integer
func5 xs ys = do
    x <- xs
    let x' = x + 1
    y <- (+1) <$> ys
    return (x' + y)

-- wrong? but at least compiles.
func5' :: Applicative f => f Integer -> f Integer -> f Integer
-- func5' fx fy =   (+) <$> ( (+ 1) <$> fx ) <*> ( (+ 1) <$> fy)
-- correct:
func5' fx fy = (\x y -> (x+1) + (y+1)) <$> fx <*> fy


func6 :: Monad f => f Integer -> f (Integer,Integer)
func6 xs = do
    x <- xs
    return $ if x > 0 then (x, 0)
                      else (0, x)
-- so ... 
func6' :: Functor f => f Integer -> f (Integer, Integer)
-- $need the () around anon function and also infix operator 
--  or you could have func6' = fmap (....)
func6' =  ( (\x -> if x > 0 then (x,0 ) else (0, x) ) <$> )  


func7 :: Monad f => f Integer -> f (Integer,Integer)
func7 xs = do
    x <- xs
    if x > 0 then return (x, 0)
             else return (0, x)

-- &*&**** 
func7' :: Functor f => f Integer -> f (Integer, Integer)
func7' =  ( (\x -> if x > 0 then (x,0 ) else (0, x) ) <$> )  


func8 :: Monad f => f Integer -> Integer -> f Integer
func8 xs x = pure (+) <*> xs <*> pure x

-- are we missing side effects if we do this? even though it is correct answer
func8' :: Functor f => f Integer -> Integer -> f Integer
func8' xs x = ( + x) <$> xs

-- alternate solution, since pure comes iwth Applicative
-- why cant you uncurry this
func8'' :: Applicative f => f Integer -> Integer -> f Integer
func8'' xs x =  pure (+) <*> xs <*> pure x 


func9 :: Monad f => f Integer -> f Integer -> f Integer -> f Integer
func9 xs ys zs = xs >>= \x -> if even x then ys else zs

-- no, because the type of <$>  is (a -> b ) -> f a -> f b
--func9' :: Applicative f => f Integer -> f Integer -> f Integer -> f Integer
--func9' xs ys zs = (\_x -> if even x then ys else zs) <$> xs   


func10 :: Monad f => f Integer -> f Integer
func10 xs = do
    x <- xs >>= (\x -> return (x * x))
    return (x + 10)

-- below is wrong... (?) cause actually you just need one <$> 
-- return has no side fx(??)
func10' :: Functor f => f Integer -> f Integer
func10' xs = (+ 10) <$> square <$> xs 
    where square x = x * x

-- correct
func10'' :: Functor f => f Integer -> f Integer
func10'' xs = (\x -> x * x + 10) <$> xs


main = do
    print "ok"
    print $ isJust $ func6 $ Just undefined
    -- error, results in undefined print $ isJust $ func7 $ Just undefined
