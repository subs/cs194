
-- e1
fibs2 :: [Integer]
fibs2 = 1:1:zipWith (+) fibs2 (drop 1 fibs2)

-- e2
data Stream a = Cons a (Stream a)

streamToList :: Stream a -> [a]
streamToList (Cons x rest) = x:streamToList rest

streamMap :: (a -> b) -> Stream a -> Stream b
streamMap f (Cons x rest) = Cons (f x) (streamMap f rest)

streamIterate :: (a -> a) -> a -> Stream a
streamIterate f seed = Cons seed $ streamIterate f $ f seed

streamInterleave :: Stream a -> Stream a -> Stream a
streamInterleave (Cons a arest) bstream = Cons a (streamInterleave bstream arest)

streamRepeat :: a -> Stream a
streamRepeat x = Cons x $ streamRepeat x

nats :: Stream Integer
nats =  streamIterate (+ 1) 1

ruler :: Stream Integer
ruler = go 0 where
  go x = streamInterleave (streamRepeat x) (go (x + 1))
-- More concise    way that was in answer:
-- ruler = streamInterleave (streamRepeat 0) (streamMap (+1) ruler)

instance Show a => Show (Stream a) where
  show = show . take 40 . streamToList 

main :: IO ()
main = do
  print $ take 50 fibs2
  print nats
  print ruler